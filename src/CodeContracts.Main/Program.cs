﻿using System;

namespace CodeContracts.Main
{
    public class Program
    {
        public static void Main()
        {
            var person = new Person("joe");
            person.SetAliases(new []{ "joel", "joeel", string.Empty});
            Console.WriteLine(person);

            Console.ReadLine();
        }
    }
}
