﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace CodeContracts.Main
{
    public class Person
    {
        private string name;
        private IEnumerable<string> aliases = new List<string>();

        [ContractInvariantMethod]
        private void Invariants()
        {
            Contract.Invariant(Contract.Exists(this.aliases, a => this.name == a));
        }

        public Person(string name)
        {
            this.name = name;
        }

        public void Rename(string newName)
        {
            this.name = newName;
        }

        public void SetAliases(IEnumerable<string> aliases)
        {
            this.aliases = aliases;
        }

        public override string ToString()
        {
            var capitalName = Char.ToUpper(this.name[0]) + this.name.Substring(1);

            return capitalName;
        }
    }
}